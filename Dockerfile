#FROM fedora:34
FROM centos:7.5.1804
#FROM centos:8.4.2105
MAINTAINER trongtb5@fpt.com.vn
LABEL Description="vsftpd Docker image based on Centos 7"
RUN yum install -y \
	vsftpd \
	ftp
COPY run-vsftpd.sh /usr/sbin/
RUN chmod +x /usr/sbin/run-vsftpd.sh
USER root
EXPOSE 21 20
EXPOSE 3000-3100
CMD ["/usr/sbin/run-vsftpd.sh"]
